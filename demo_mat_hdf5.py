def read_mat(infile):
    """read Matlab input

    :param infile: input file (str)
    :return: mat_dict
    """
    from scipy.io import loadmat
    d = loadmat(infile)

    return d

def read_hdf5(infile):
    """read HDF5 input

    :param infile: input file (str)
    :return: mat_file_ref
    """
    import h5py

    f = h5py.File(infile)

    return f

def main():
    args = parse_read_file()

    infile = args.infile

    try:
        d = read_mat(infile)
    except:
        d = read_hdf5(infile)

    dictionary = dict([('params', d['params']), ('x', d['x']), ('y', d['y'])])

    return dictionary

def parse_read_file():
    """parse read .mat file

    :returns: args
    """
    import argparse as ap

    par = ap.ArgumentParser(description="load .mat file and return dictionary",
                            formatter_class=ap.ArgumentDefaultsHelpFormatter)

    par.add_argument("--infile",
                     dest="infile",
                     help=".mat filename",
                     default='mat_v5.mat')

    args = par.parse_args()

    return args

if __name__ == "__main__":
    d = main()
    print(d)

